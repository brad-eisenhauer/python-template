import unittest

from {{cookiecutter.module_name}}.hello import say_hello


class SayHelloTests(unittest.TestCase):
    def test_say_hello(self):
        self.assertEqual("Hello, Mr. Anderson!", say_hello("Mr. Anderson"))

