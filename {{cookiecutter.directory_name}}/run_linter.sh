#!/usr/bin/env sh

flake8 . --extend-exclude=dist,build --show-source --statistics
mypy {{cookiecutter.module_name}}

